package cz.ill_j.files;

public enum FileExtensionEnum {

	TXT (".txt"),
	XML (".xml"),
	JSON (".json");
	
	private String value;
	
	/**
	 * Constructor
	 * @param value
	 */
	private FileExtensionEnum (String value) {
		this.value = value;
	}
	
	public String toString () {
		return this.value;
	}
}
