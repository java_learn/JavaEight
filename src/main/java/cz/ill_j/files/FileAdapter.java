package cz.ill_j.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileAdapter {
	
	private String path;
	
	private String fileName;
	
	/**
	 * {@link Constructor} init fields in default.
	 */
	public FileAdapter() {
		this.path = System.getenv("APPDATA") + File.separator + "LearnJava";
		this.fileName = "LearJavaTest.txt";
	}
	
	/**
	 * {@link Constructor} init fields by params.
	 * @param path  
	 * @param fileName 
	 * @param FileExtensionEnum item
	 */
	public FileAdapter(String path, String fileName, FileExtensionEnum item) {
		this.path = path;
		this.fileName = fileName + item;
	}

	public Stream<?> getFileStream () throws IOException {
		return Files.lines(Paths.get(path + File.separator + fileName));
	}
	
	public String getFileName () {
		return fileName;
	}
	
	public void writeToFile (String line) {
		Charset charset = Charset.forName("US-ASCII");
		
//		try (BufferedWriter writer = Files.newBufferedWriter(file, charset)) {
//		    writer.write(line, 0, line.length());
//		    
//		} catch (IOException x) {
//		    System.err.format("IOException: %s%n", x);
//		}
	}
	
	public void readFile () {
		
	}
	
	/**
	 * use localy only, when Path is specified.
	 * @return
	 */
	private boolean isFileExist () {
		if (Files.exists(Paths.get(path + File.separator + fileName))) {
			return true;
		}
		return false;
	}
	
}
