package cz.ill_j.javaeight.exceptions;

import java.io.File;
import java.io.FileNotFoundException;

public class HowExceptionsWork {

	/**
	 * we expect ArrayIndexOutOfBoundsException
	 */
	public void throwsArrayIndexOutOfBoundsException () {
		String[] animals = new String[0];
		System.out.println(animals[0]);
	}
	
	public void newExceptionMessage () throws Exception {
		throw new Exception ("pad na prdel");
	}
	
	public void exceptionCascade () {
		try {
			File file = new File("");
		} catch (IllegalArgumentException ilex) {
			
		} catch (RuntimeException rex) {
			
		} catch (Exception ex) {
			
		}
	}
	
	
	
}
