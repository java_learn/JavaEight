package cz.ill_j.javaeight.exceptions;

public class HowErrorsWork {

	// expected:
	// Exception in thread "main" java.lang.ExceptionInInitializerError
	// Caused by: java.lang.ArrayIndexOutOfBoundsException: -1
	static {
		int[] countsOfMoose = new int[3];
		int num = countsOfMoose[-1];
		}
	
	public static void main(String[] args) {
		System.out.println("Error nevybehl.");
	}
	
	public void getStackOverflowException () throws StackOverflowError {
		throw new StackOverflowError();		
	}
}
