package cz.ill_j.javaeight.exceptions;

public class NullPointerWork {

    private String message = null;

    /**
     * Start NullPointerException testing process
     * @param args
     */
    public static void main(String[] args) {
        NullPointerWork instance = new NullPointerWork();
        instance.testOne();
    }

    private void testOne () {
        try {
            if (message == null) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            System.out.println("Catch. NullPointerException was thrown programatically.");
        } finally {
            System.out.println("This should be shown allways. Finally part.");
        }
    }
}
