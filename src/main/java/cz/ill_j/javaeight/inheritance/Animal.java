package cz.ill_j.javaeight.inheritance;

public class Animal {

	private int age;
	
	public Animal (int age) {
		super();
		this.age = age;
		System.out.println("Animal int argument constructor.");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	private String getName () {
		return "String";
	}
	

	// methods for overload and override
	
	public Animal getAnimalObject () {
		return null;
	}
}
