package cz.ill_j.javaeight.inheritance;

public class Ape extends Animal {

	public Ape () {
		this(12);
		System.out.println("Ape non argument constructor.");
	}
	
	public Ape (int age) {
		super(age);
		System.out.println("Ape one int argument constructor.");
	}
	
	private void roar() {
		System.out.println("The " + getAge() + " year old lion says: Roar!");
	}
	
	public String getName() {
		return "";
	}
	public int getName(int s) {return s;}
	
	// methods for overload and override
	
	public Ape getAnimalObject () {
		return null;
	}
}
