package cz.ill_j.javaeight.lookingword;

public class WordGenerator {

	static int wordLenght = 5;
	static char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	String[] result;
	String newWord = "";

	
	public static void main(String[] args) {
		
		StringBuilder sb = new StringBuilder();
        for (int length = 2; length <= wordLenght; length++) {
            sb.setLength(length);
            createNewWord(sb, 0);
        }
	}

	/**
	 * rekurzivni metoda
	 * @param sb
	 * @param n
	 */
	public static void createNewWord(StringBuilder sb, int n) {

		if (n == sb.length()) {
	            System.out.println(sb.toString());
	            // TODO: save to file 
	            return;
	        }
        for (char letter : alphabet) {
            sb.setCharAt(n, letter);
            createNewWord(sb, n + 1);
        }
	}
}
