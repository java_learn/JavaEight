package cz.ill_j.javaeight.lookingword;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class AllWordsArray {

	/**
	 * <b> getAllWordsArray() </b> <br>
	 * creates ArrayList filled by all czech words from dictionary, *.txt file
	 * 
	 * @return
	 */
	public ArrayList<String> getAllWordsArray() {

		// file address declaration
		String address = "src" + File.separator + "main" + File.separator + "resources" + File.separator
				+ "czechdictionary";
		File dictionaryQualifiedName = new File(address + File.separator + "slovnik1.txt");

		System.out.println("Does this file exists? " + dictionaryQualifiedName.exists());
		System.out.println(dictionaryQualifiedName);

		// check if you can continue
		if (!dictionaryQualifiedName.exists()) {
			return null;
		}

		// reading the file and create ArrayList
		BufferedReader br;
		ArrayList<String> wordList = new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader(dictionaryQualifiedName));

			do {
				wordList.add(br.readLine());
			} while (br.ready());
			//
			br.close();

			System.out.println("ArrayList<String>: wordList.size() " + wordList.size());

		} catch (Exception e) {
			//
			e.printStackTrace();
		}

		return wordList;
	}

}
