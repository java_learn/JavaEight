package cz.ill_j.javaeight.lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class LambdaLearn {

	private static final int INPUT = 20;

	public static void main(String[] args) {
		lambdaLongVersion(INPUT);

		testStream();

		// lambda Animal
		List<Animal> animals = new ArrayList<Animal>(); // list of animals
		animals.add(new Animal("fish", false, true));
		animals.add(new Animal("kangaroo", true, false));
		animals.add(new Animal("rabbit", true, false));
		animals.add(new Animal("turtle", false, true));
		animals.add(new Animal("snake", false, false));

//		print(animals, new CheckIfHopper()); // pass class that does check
		print(animals, a -> a.canHop());
		print(animals, a -> a.canSwim());
		print(animals, a -> ! a.canSwim());
		
		System.out.println(animals);
		animals.removeIf(s -> s.toString().charAt(0) == 'r');
		System.out.println(animals);
	}
	
	
	

	public static void lambdaLongVersion(int x) {
		System.out.println(multipleLongVersion.countNow(x));
	}

	public static MyFunction multipleLongVersion = (int i) -> {
		return i * i;
	};

	public MyFunction multipleShortVersion = (i) -> i * i;

	public MyFunction multipleWithoutBrackets = i -> i * i;

	// java.util.function.Supplier
	public Supplier<Integer> dodavatel = () -> 123;

	public void biFunctionExample() {
		BiFunction<String, Integer, String> doubleFunction = (s, i) -> s + ": " + i;
	}

	// stream with lambda and collections
	public static void testStream() {
		Collection<String> letters = Arrays.asList(new String[] { "a", "b", "c" });

		Stream<String> proud = letters.stream();
		proud.forEach(p -> System.out.println(p)); // p je typu String

		// premapovani
		letters.stream().map(p -> p.toUpperCase()).forEach(s -> System.out.println(s));
	}

	// Animal support methods
	private static void print(List<Animal> animals, Predicate<Animal> checker) {
		for (Animal animal : animals) {
			if (checker.test(animal)) // the general check
				System.out.print(animal + " ");
		}
		System.out.println();
	}

}
