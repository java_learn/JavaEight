package cz.ill_j.javaeight.lambdas;

@FunctionalInterface
public interface MyFunction {

	public int countNow (int input);
}
