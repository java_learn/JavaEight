package cz.ill_j.javaeight.javabuildingblocs;

import java.util.ArrayList;
import java.util.List;

public class Statics {

	private static final int NUM_BUCKETS = 45;
	private static final List<String> values = new ArrayList<>();
	
	private static int one;
	private static final int two;
	private static final int three = 3;
	private static final int four;
	private static int six;
	
	// static initializer
	static {
		one = 1;
		two = 2;
		four = 4;
	}
	
	//instance initializer
	{
		System.out.println("instance initializer called");
	}
	
	public static void main(String[] args) {

		Koala.count = 4;
		Koala koala1 = new Koala();
		Koala koala2 = new Koala();
		koala1.count = 6;
		koala2.count = 5;
		System.out.println(Koala.count);

		values.add("gogo");
		values.add("stop");
		for (String value : values) {System.out.println(value);}
		
		new Statics();
		Statics.six = two * three;
		System.out.println(one + two + three + six);
		
		int num = 4;
		newNumber(5);
	}

	private static void newNumber(int num) {
		num = 8;		// only local variable
	}
	
	


}
