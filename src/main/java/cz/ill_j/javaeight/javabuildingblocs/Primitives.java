package cz.ill_j.javaeight.javabuildingblocs;

public class Primitives {

	private int maxValue = 2147483647;
	private int minValue = -2147483648;
	private int underscoreMilion = 1_000_000;

	public int getMaxValue() {
		return maxValue;
	}

	public int getMinValue() {
		return minValue;
	}
	
	/**
	 * 
	 * @param input
	 * @return long value
	 */
	public long parseToLongType (int input) {
		return input*maxValue;
	}
	
	/**
	 * 
	 * @return 1_000_000
	 */
	public int getUnderscoreMilion () {
		return underscoreMilion;
	}
}
