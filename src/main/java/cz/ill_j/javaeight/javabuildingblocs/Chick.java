package cz.ill_j.javaeight.javabuildingblocs;

public class Chick {

	public int numEggs = 0;
	String name;

	/**
	 * constructor has the name matched with class name and misses return
	 * statement
	 */
	public Chick() {
		name = "Duke"; // initialize in constructor
		System.out.println("in constructor");
	}
}
