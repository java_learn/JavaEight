package cz.ill_j.javaeight.javabuildingblocs;

import java.time.Period;
import java.util.Arrays;

public class BinarySearch {

	static int [] numbers = {9,2,1,8,7,};
	static int searchNum = 5;
	
	public static void main(String[] args) {
	
		System.out.println("Looking for number: " + searchNum);
		
		// SCAZY!
		Arrays.sort(numbers);
		System.out.println("Sorted array: ");
		for (int number : numbers) {
			System.out.println(number);
		}
		int a = Arrays.binarySearch(numbers, searchNum);
		System.out.printf("Arrays.binarySearch(numbers, %s) = ", searchNum);
		System.out.println(a);
		
		// period od Month and Day
		Period.of(0, 1, 1);
	}


}
