package cz.ill_j.javaeight.javabuildingblocs;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringExamples {

	public static void main(String[] args) {

		StringExamples examples = new StringExamples();
		
		// compare concate
		examples.stringTime();
		examples.stringBufferTime();

		try {
		examples.arrayListExample();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// List<> example
		examples.sortList();
	}

	public void stringBufferTime () {
		
		int startTime = LocalTime.now().getNano();
		
		StringBuilder alpha = new StringBuilder();
		for(char current = 'a'; current <= 'z'; current++)
			alpha.append(current);
		System.out.println(alpha);
		
		int stopTime = LocalTime.now().getNano();
		System.out.println("stringBufferTime () took " + (stopTime - startTime) + " nanoseconds.");
		
		}

	public void stringTime () {
        // String testing, alpha is new object in each iteration.
		
		int startTime = LocalTime.now().getNano();
		
        String alpha = "";
        for(char current = 'a'; current <= 'z'; current++)
        	alpha += current;
       	System.out.println(alpha);
       	
		int stopTime = LocalTime.now().getNano();
		System.out.println("stringTime () took " + (stopTime - startTime) + " nanoseconds.");
	}
	
	public void arrayListExample () throws UnsupportedOperationException {
		
		String[] array = { "hawk", "robin" }; // [hawk, robin]
		List<String> list = Arrays.asList(array); // returns fixed size list
		System.out.println(list.size()); // 2
		list.set(1, "test"); // [hawk, test]
		array[0] = "new"; // [new, test]
		
		for (String b : array) {
			System.out.println(b); // new test
		}
		list.remove(1); // throws UnsupportedOperation Exception
	}
	
	public void sortList () {
		
		List<Integer> numbers = new ArrayList<>();
		numbers.add(99);
		numbers.add(5);
		numbers.add(81);
		System.out.println("Sorting List<Integer> " + numbers);
		
		Collections.sort(numbers);
		System.out.println(numbers);
	}
}
