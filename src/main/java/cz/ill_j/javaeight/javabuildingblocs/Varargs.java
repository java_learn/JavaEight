package cz.ill_j.javaeight.javabuildingblocs;

public class Varargs {
	
	/**
	 * main method ready for everyday usage, testing, prototyping
	 * @param args
	 */
	public static void main(String[] args) {
		useVarArgs(1);
		useVarArgs(1, 5, 8, 4);
		useVarArgs(1, new int[] {4, 5});
		// throws NullPointerException
		//		useVarArgs(1, null);   
		
		useVarArg("I wanted to", " use", " String... varargs.");
	}
	
	//there are some overloaded methods
	/**
	 * it prints array length of varargs
	 * @param start
	 * @param nums
	 */
	public static void useVarArgs (int start, int... nums) {
		System.out.println(nums.length);
	} 
	
	public static void useVarArg(String... values) {
		for (String value : values) {
			System.out.println(value);
		}
	}
}
