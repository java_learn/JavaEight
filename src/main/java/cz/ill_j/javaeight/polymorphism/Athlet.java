package cz.ill_j.javaeight.polymorphism;

public class Athlet implements CanSwim, CanRun {

	public static void main(String[] args) {
		// callinig default method in interface
		System.out.println(new Athlet().getMaxDepth());
		System.out.println(new Athlet().getSpeed());
	}

	@Override
	public int getSpeed() {
		
		return getMaxDepth()*2;
	}

}
