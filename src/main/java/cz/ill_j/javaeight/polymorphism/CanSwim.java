package cz.ill_j.javaeight.polymorphism;

public interface CanSwim {

	public static final int MAXIMUM_DEPTH = 100;

	public int getSpeed();
	
	public default int getMaxDepth() {
		return MAXIMUM_DEPTH;
	}
}
