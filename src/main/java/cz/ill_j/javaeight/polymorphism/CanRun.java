package cz.ill_j.javaeight.polymorphism;

public interface CanRun {

	public int getSpeed();
}
