package cz.ill_j.javaeight.constructors;


public class Constructors {

	// constructors
	Bunny bunny = new Bunny();
	
	// other storry
	Rabbit1 r1 = new Rabbit1();
	Rabbit2 r2 = new Rabbit2();
	Rabbit3 r3 = new Rabbit3(true);
//	Rabbit4 r4 = new Rabbit4();     -- doen't compile due to private constructor, 
	
	
	// encapsulation
	private int numberEggs; // private

	public int getNumberEggs() { // getter
		return numberEggs;
	}

	public void setNumberEggs(int numberEggs) { // setter
		if (numberEggs >= 0) // guard condition
			this.numberEggs = numberEggs;
	}

	// immutable
}

class Rabbit1 {
}

class Rabbit2 {
	public Rabbit2() {
	}
}

class Rabbit3 {
	public Rabbit3(boolean b) {
	}
}

class Rabbit4 {
	private Rabbit4() {
	}
}