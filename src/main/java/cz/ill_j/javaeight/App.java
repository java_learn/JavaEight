package cz.ill_j.javaeight;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.ill_j.javaeight.constructors.Bunny;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		int x = 0;
		while (++x < 5) {
			x += 1;
		}
		String message = x > 5 ? "Greater than" : "Less Than";
		System.out.println(message + "," + x);
		
		int[][] times = new int[3][5];
		for (int i = 0; i < times.length; i++) 
			for (int j = 0; j < times[i].length; j++)
				times[i][j] = i*j;
		System.out.println(times[2][4]);

	}

	////////////////////////////////////////

	// testing methods

	public void getAritmenticException() {
		try {
			System.out.println(123 / 0);

		} catch (ArithmeticException e) {
			e.printStackTrace();
		} catch (RuntimeException ed) {

		}
	}

	public void question1() {
		String year = new String("Senior");
		switch (year) {
		case "junior":
		case "nothing":
		case "Senior":
			System.out.println("nasel jsem");
			break;
		default:
			System.out.println("default printed");
		}

		String t = new String(year);
		if ("Senior".equals(t)) {
			System.out.println("Senior.equals(t)" + (int) 'a');
		}

		int[][] times = new int[3][3];
		String ref = null;
		System.out.println(ref);

		if (4 < 5 ^ 2 == 2) {
		}

		LocalDateTime dateTime = LocalDateTime.of(2015, 5, 10, 11, 22, 33);
		Period p = Period.of(1, 2, 3);
		dateTime.minus(p);

		StringBuilder b;
		StringBuffer bf;

		// NullPointerEx.
		// Integer in = null;
		// int it = 0;
		// if (in == it) {}

		List differentTypes = new ArrayList();
		differentTypes.add(dateTime);

		String[] asd = { "prask", "ok" };
		Object obj = asd;
		List<String> listAsd = Arrays.asList(asd);
		System.out.println(listAsd);

		List<Integer> fds = new ArrayList();
	}
}
