package cz.ill_j.javaeight.exceptions;

import static org.junit.Assert.*;
import org.junit.Test;

public class HowExceptionsWorkTest {

	private HowExceptionsWork howExceptionsWork = new HowExceptionsWork();
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionTest () {
		howExceptionsWork.throwsArrayIndexOutOfBoundsException();
	}
	
	@Test
	public void newExceptionMessageTest () {
		boolean catchBlockRun = false;
		
		try {
			howExceptionsWork.newExceptionMessage();
		} catch (Exception e) {
			
			e.printStackTrace();
			assertTrue(!e.getMessage().isEmpty());
			catchBlockRun = true;
		}
		
		assertTrue(catchBlockRun);
	}
}
