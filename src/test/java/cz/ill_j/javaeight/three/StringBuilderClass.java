package cz.ill_j.javaeight.three;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringBuilderClass {

	@Test
	public void testStringBuilder() {
		
		// String is immutable
		String alpha = "";
		for (char current = 'a'; current <= 'z'; current++)
			alpha += current;
		System.out.println(alpha);

		// StringBuilder is mutable
		StringBuilder beta = new StringBuilder();
		for (char current = 'a'; current <= 'z'; current++)
			beta.append(current);
		System.out.println(beta);
		
		// compare String and StringBuilder !
		assertEquals(alpha, beta.toString());
	}

}
