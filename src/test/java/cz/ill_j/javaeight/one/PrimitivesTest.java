package cz.ill_j.javaeight.one;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import cz.ill_j.javaeight.javabuildingblocs.Primitives;

public class PrimitivesTest {

	@Test
	public void intMaxValueTest() {
		Primitives primitive = new Primitives();
		Assert.assertEquals(Integer.MAX_VALUE, primitive.getMaxValue());
	}

	@Test
	public void intMinValueTest() {
		Primitives primitive = new Primitives();
		Assert.assertEquals(Integer.MIN_VALUE, primitive.getMinValue());
		// System.out.println(Integer.MIN_VALUE);
	}

	/**
	 * multiplicates int max value, so we have to use long type
	 */
	@Test
	public void parseToLongTypeTest() {
		int multiplicationValue = 12;
		Primitives primitive = new Primitives();

		// primitive.parseToLongType(multiplicationValue)
		assertTrue(checkIfLong(primitive.parseToLongType(multiplicationValue)));

		long a = 2000L;
		long b = 2000l;

		Assert.assertEquals("Primitive Long is defined with l suffix.", a, new Long(2000).longValue());
		Assert.assertEquals("Does not matter if L is either capital or lowercase.", (a == b), Boolean.TRUE);
	}

	private boolean checkIfLong(int value) {
		return false;
	}

	private boolean checkIfLong(long value) {
		return true;
	}

	/**
	 * check if we can use underscored numeric literals allowed since java 1.7
	 * DON'T use by decimal point, at begin, at end
	 */
	@Test
	public void getUnderscoreMilionTest() {
		int oneMilion = 1000000;
		Primitives primitive = new Primitives();

		Assert.assertEquals(primitive.getUnderscoreMilion(), oneMilion);
	}
}
