package cz.ill_j.javaeight.two;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class ArithmeticTest {
	
	int a = 2;
	int b = 5;
	int c = 3;
	int d = 8;
	int e = 4;
		
	int x = a * b;
	int xx = b + c;
	int y = c * d;
	int z = -e;

	// a * b + c * d - e 
		
	public int result1 () {
		return a * b + c * d - e;
	}
	public int result2 () {
		return x + y + z;
	}
	
	public int result3 () {
		return a * ((b + c) * d - e);
	}
	public int result4 () {
		return a * (xx * d - e);
	}
	
	// Working with Binary Arithmetic Operators
	
	/**
	 * testing preferences within operators;  
	 * adding and multiplication; 
	 * brackets; 
	 */
	@Test
	public void precedence() {
		assertTrue(result1() == result2());
		assertTrue(result3() == result4());
	}
	
	/**
	 * promotion of data type
	 */
	@Test
	public void promotion () {
		short sh1 = 10;
		short sh2 = 3;
		// note, that multiplicated short variables are promoted into int
		int sh3 = sh1 * sh2;
	}
	
	// Working with Unary Operators
	
	@Test
	public void increment () {
		y = ++x * 5 / x-- + --x;
		System.out.println("x is " + x);
		System.out.println("y is " + y);
	}
	
	@Test
	public void castValue () {
		short i = (short)19212225;
		System.out.println(i);
	}
	
	/**
	 * to wrap to the next negative number; <br>
	 * for example:   System.out.print(2147483647+1); // print -2147483648
	 */
	@Test
	public void intWrap () {
		for (int i = 1; i < 100; i++) {
			assertTrue(Integer.MAX_VALUE + i == Integer.MIN_VALUE + i - 1);
		}
	}
	
	// Compound Assignment Operators
	
	@Test
	public void compound () {
		long i = 5;
		int j = 8;
		int k = (int)i*j;  // i*j does not compile !!
		
		assertTrue(k == (i *= j));
		
		j = (k = 5);
		assertTrue(j == k);  // assignment is an expression in and of itself
	}
	
	// @Test
	public void relationOperatorsTest () {
		// < <= > >=   it's clear
		
		// a instanceof b
		
	}
	
	class A { }  
	class C extends A { } 
	class D extends A { } 

	@Test
	public void testInstance(){
	    A c = new C();
	    A d = new D();
	    assertTrue(c instanceof A && d instanceof A);
	    assertTrue(c instanceof C && d instanceof D);
	    assertFalse(c instanceof D);
	    assertFalse(d instanceof C);
	}
	
	
	
	
	
	
	
	
}
