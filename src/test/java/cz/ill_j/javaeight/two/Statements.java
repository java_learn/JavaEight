package cz.ill_j.javaeight.two;

import static org.junit.Assert.*;

import org.junit.Test;

public class Statements {

	/**
	 * comparison of two ways of statements
	 */
	@Test
	public void ternaryOperatorTest() {
		// booleanExpression ? expression1 : expression2

		int y = 10;
		final int x;
		if (y > 5) {
			x = 2 * y;
		} else {
			x = 3 * y;
		}

		assertTrue(x == ternaryExpression(y));

	}

	/**
	 * we use ternary operator to get number
	 * 
	 * @param y
	 * @return
	 */
	private int ternaryExpression(int y) {

		return (y > 5) ? (2 * y) : (3 * y);
	}

	@Test
	public void testSwitchWithEnum() {

		MotoEnum myBike = MotoEnum.YAMAHA;
		MotoEnum resultValue = null;

		switch (myBike) {
		case APRILIA:
			resultValue = MotoEnum.APRILIA;
			break;
		case BMW:
			resultValue = MotoEnum.BMW;
			break;
		case CAGIVA:
			resultValue = MotoEnum.CAGIVA;
			break;
		case HONDA:
			resultValue = MotoEnum.HONDA;
			break;
		case KAWASAKI:
			resultValue = MotoEnum.KAWASAKI;
			break;
		case YAMAHA:
			resultValue = MotoEnum.YAMAHA;
			break;
		default:
			break;
		}

		assertTrue("Switch statement works as needed", myBike.equals(resultValue));
	}

	// TODO; I'm not sure if it's right
	@Test
	public void testWhileLoop() {

		int x = 15;
		int y = x;
		int numberOfTheLoops = 0;

		while (x > 10) {
			x--;
			numberOfTheLoops++;
		}

		System.out.println("numberOfTheLoops " + numberOfTheLoops);
		System.out.println("x " + x);

		do {
			y--;
			numberOfTheLoops--;
		} while (numberOfTheLoops >= 0); // look at this...

		System.out.println("numberOfTheLoops " + numberOfTheLoops);
		System.out.println("y " + y);

		assertNotEquals(x, y);
	}

	// Creating an Infinite Loop
	/*
	 * for( ; ; ) { System.out.println("Hello World"); }
	 */

	@Test
	public void testBasicForLoop() {

		int countOfIterations = 0;

		for (int i = 1; i <= 10; i++) {
			countOfIterations++;
		}
		assertEquals("Count of iteration must be 10", 10, countOfIterations);
	}

	/**
	 * FOR EACH (enhanced for loop)<br>
	 * it has to iterate via collection, Array or ArrayList
	 */
	@Test
	public void testForEachLoop() {

		int countOfIterations1 = 0;
		int countOfIterations2 = 0;

		String[] names = new String[3];
		for (String name : names) {
			// print 3 times null
			countOfIterations1++;
			System.out.print(name + " ");
		}

		for (int i = 0; i < names.length; i++) {
			countOfIterations2++;
		}

		assertTrue(countOfIterations1 == countOfIterations2);

		
		// break statement with LABEL
		int[][] list = { { 1, 13, 5 }, { 1, 2, 5 }, { 2, 7, 2 } };
		int searchValue = 2;
		int positionX = -1;
		int positionY = -1;
		PARENT_LOOP: for (int i = 0; i < list.length; i++) {
			for (int j = 0; j < list[i].length; j++) {
				if (list[i][j] == searchValue) {
					positionX = i;
					positionY = j;
					break PARENT_LOOP;
				}
			}
		}
		System.out.println("Value "+searchValue+" found at: " +
				"("+positionX+","+positionY+")");
		
	}

}
