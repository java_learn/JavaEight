package cz.ill_j.javaeight.junit;

import org.junit.Test;
import static org.junit.Assert.*;

public class MyUnitTest {

	@Test
	public void testConcatenate() {
		MyUnit myUnit = new MyUnit();
		String result = myUnit.concatenate("one", "two");
		assertEquals("onetwo", result);
	}
	
	@Test
	public void testRegex () {
		
		String pattern = "@javax.persistence.Table(name=\\[a-zA-Z\\],";
		String mattcher = "@javax.persistence.Table(name=USERS,";
		
		assertTrue(mattcher.equals(pattern));
	}
}
